﻿namespace Experiments.Interfaces
{
    public interface ISuccessResult:IResult
    {
        
    }

    public interface ISuccessResult<T> : ISuccessResult, IResult<T>
    {
        T Value { get; }
    }
}