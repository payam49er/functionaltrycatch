﻿using System;

namespace Experiments.Interfaces
{
    public interface IFailureResult:INonResult
    {
        Exception Exception { get; }
    }

    internal interface IFailureResult<T> : IFailureResult, INonResult<T>
    {
        
    }
}