﻿namespace Experiments
{
    public interface INonResult:IResult
    {
        
    }

    internal interface INonResult<T> : INonResult, IResult<T>
    {
        
    }
}