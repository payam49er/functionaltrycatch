﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Experiments.Interfaces;
using Experiments.Result_Types;

namespace Experiments
{
    public static partial class Result
    {
        public static IResult<T> Return<T>(T value) => value == null ? 
            Return<T>() :new SuccessResult<T>(value);

        public static IResult<T> Return<T>() => new NoneResult<T>();

        public static IResult Return() => new NoneResult();
        public static IResult<T> Return<T>(IResult<T> value) => value;

        public static IResult<T> Return<T, TException>(TException exception) where TException : Exception
            => new FailureResult<T>(exception);

        internal static IFailureResult<T> Return<T>(IFailureResult failure) => new FailureResult<T>(failure);

    }
}
