﻿using System;
using System.Collections;
using System.Runtime;
using Experiments.Interfaces;

namespace Experiments.Result_Types
{
    public static class ResultExtension
    {
        public static IResult<TOut> Bind<T,TOut>(this IResult<T> result,Func<T,IResult<TOut>> binder)
        {
            switch (result)
            {
                    case ISuccessResult<T> success:
                        return Result.Wrap(() => binder(success.Value));
                    case IFailureResult failure:
                         return Result.Return<TOut>(failure);
                    default:
                        return Result.Return<TOut>();
            }   
        }


        public static bool Exists<T>(this IResult<T> result, Predicate<T> predicate) =>
            result.Filter(predicate).IsSuccess();


        public static IResult<T> Filter<T>(this IResult<T> result, Predicate<T> filter)
        {
            if (!(result is ISuccessResult<T> success)) return result;

            switch (Result.Wrap(()=>filter(success.Value)))
            {
                   case ISuccessResult<bool> filterSuccess:
                       return filterSuccess.Value ? success : Result.Return<T>();
                   case IFailureResult filterFailure:
                       return Result.Return<T>(filterFailure);
                   default:
                       return Result.Return<T>();
            }
        }

        public static int Count(this IResult result) => result.IsSuccess() ? 1 : 0;

        public static TOut Fold<T, TOut>(this IResult<T> result, Func<TOut, T, TOut> folder, TOut initialState)
        {
            if (!(result is ISuccessResult<T> success)) return initialState;
            try
            {
                return folder(initialState, success.Value);
            }
            catch (Exception e)
            {
                 throw new ResultException("Folder function threw an exception",e);    
            }
        }

        public static bool IsFailure(this IResult result) => result is INonResult;

        public static bool IsNone(this IResult result) => result is INonResult;

        public static bool IsSuccess(this IResult result) => result is ISuccessResult;

        public static void Iter<T>(this IResult<T> result, Action<T> action)
        {
            if (!(result is ISuccessResult<T> success)) return;
            try
            {
                action(success.Value);
            }
            catch (Exception e)
            {
                throw new ResultException("Iter action threw an exception.",e);
            }
        }

        public static IResult<TOut> Map<T, TOut>(this IResult<T> result, Func<T,TOut> mapping)
        {
            switch (result)
            {
                    case ISuccessResult<T> success:
                        return Result.Wrap(() => mapping(success.Value));
                    case IFailureResult failure:
                        return Result.Return<TOut>(failure);
                    default:
                        return Result.Return<TOut>();
            }
        }
    }
}