﻿using System;
using Experiments.Interfaces;

namespace Experiments.Result_Types
{
    
    internal sealed class FailureResult<T> : FailureResult,IFailureResult<T>
    {
        public FailureResult(Exception exception) : base(exception)
        {
            
        }

        public FailureResult(IFailureResult failure) : base(failure)
        {
            
        }
    }


    internal class FailureResult : IFailureResult
    {
        public Exception Exception { get; }
        
        
        public FailureResult(Exception exception)
        {
            Exception = exception ?? new Exception("Null Exception passed");
            Result.Logger(exception);
        }


        protected FailureResult(IFailureResult failure) => Exception = failure.Exception;
    }
}