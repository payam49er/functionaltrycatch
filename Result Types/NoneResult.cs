﻿namespace Experiments.Result_Types
{
    internal sealed class NoneResult<T>:INonResult<T>
    {
            
    }

    internal sealed class NoneResult : INonResult
    {
        
    }
}