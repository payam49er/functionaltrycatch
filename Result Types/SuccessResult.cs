﻿using Experiments.Interfaces;

namespace Experiments.Result_Types
{
    internal sealed class SuccessResult<T>:ISuccessResult<T>
    {
        public SuccessResult(T value) => Value = value;
        public T Value { get; }
    }
}