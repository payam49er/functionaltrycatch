﻿using System;

namespace Experiments.Result_Types
{
    public sealed class ResultException:Exception
    {
        public ResultException(){}

        public ResultException(string message) : base(message){}
        public ResultException(string message,Exception innerException):base(message,innerException){}
    }
}