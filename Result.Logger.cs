﻿using System;

namespace Experiments
{
    public static partial class Result
    {
        internal static Action<Exception> Logger { get; private set; } = exception => { };


        public static void SetLogger(Action<Exception> action)
        {
            Logger = action;
        }
    }
}