﻿using System;
using Experiments.Interfaces;
using Experiments.Result_Types;

namespace Experiments
{
    public static partial class Result
    {
        public static IResult<T> Wrap<T>(Func<T> function, Func<Exception, Exception> exceptionHandler = null)
        {
            try
            {
                return Return(function());
            }
            catch (Exception ex)
            {
                return Wrap<T>(exceptionHandler, ex);
            }
        }


        internal static IResult<T> Wrap<T>(Func<IResult<T>> function,
            Func<Exception, Exception> exceptionHandler = null)
        {
            try
            {
                return Return(function());
            }
            catch(Exception ex)
            {
                return Wrap<T>(exceptionHandler, ex);
            }
        }

        public static IResult<bool> Wrap(Action action, Func<Exception, Exception> exceptionHandler = null)
        {
            try
            {
                action();
                return Return(true);
            }
            catch (Exception ex)
            {
                return Wrap<bool>(exceptionHandler, ex);
            }
        }

        private static IFailureResult<T> Wrap<T>(Func<Exception, Exception> exceptionHandler, Exception exception)
        {
            var handler = exceptionHandler ?? (e => e);
            try
            {
                return (IFailureResult<T>) Return<T, Exception>(handler(exception));
            }
            catch (Exception ex)
            {
                return (IFailureResult<T>) Return<T, Exception>(
                    new ResultException("Exception Handler threw an exception!", ex));

            }
        }
    }
    
    
}